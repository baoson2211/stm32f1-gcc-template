# README #

My template project for STM32F103xB.
Using SPL, auto build with make and require arm-none-eabi-gcc-4.8.4 or newer.

### Tree ###
```
.
├── Libraries     => Standard peripheral libraries
├── Inc           => user header files
└── Src           => user source files
```

### Clock Configuration with High-Speed External ###

a. Correct frequency of High-Speed External clock source in **stm32f10x.h**, line **119**:
```
#!c
#define HSE_VALUE    ((uint32_t)12000000)
```

b. If you use an 8MHz crystal, uncommnet the line defined follow system clock freq you want in **system_stm32f10x.c**, line **106** to line **116**. In this case, i chose system clock freq = 72MHz:
```
#!c
#if defined (STM32F10X_LD_VL) || (defined STM32F10X_MD_VL) || (defined STM32F10X_HD_VL)
/* #define SYSCLK_FREQ_HSE    HSE_VALUE */
 #define SYSCLK_FREQ_24MHz  24000000
#else
/* #define SYSCLK_FREQ_HSE    HSE_VALUE */
/* #define SYSCLK_FREQ_24MHz  24000000 */ 
/* #define SYSCLK_FREQ_36MHz  36000000 */
/* #define SYSCLK_FREQ_48MHz  48000000 */
/* #define SYSCLK_FREQ_56MHz  56000000 */
#define SYSCLK_FREQ_72MHz  72000000
#endif
```

c. If you have an 8MHz crystal, this step is unnecessary. If not, go to function **SetSysClockToXyz()**, where **Xyz** is equally system clock frequency (e.g: HSE, 24, 36, 48, 56 or 72), which you chose at **step b**. Simply, you can use STM32CubeMX to learn how to config clock correctly and implement in your source code. Example, i still choose sysclk is 72MHz, crystal 12MHz and use **STM32F103RBT6** MCU:
```
#!c
static void SetSysClockTo72(void)
{
  __IO uint32_t StartUpCounter = 0, HSEStatus = 0;
  
  /* SYSCLK, HCLK, PCLK2 and PCLK1 configuration ---------------------------*/    
  /* Enable HSE */    
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);
 
  /* Wait till HSE is ready and if Time out is reached exit */
  do
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;  
  } while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

  if ((RCC->CR & RCC_CR_HSERDY) != RESET)
  {
    HSEStatus = (uint32_t)0x01;
  }
  else
  {
    HSEStatus = (uint32_t)0x00;
  }  

  if (HSEStatus == (uint32_t)0x01)
  {
    /* Enable Prefetch Buffer */
    FLASH->ACR |= FLASH_ACR_PRFTBE;

    /* Flash 2 wait state */
    FLASH->ACR &= (uint32_t)((uint32_t)~FLASH_ACR_LATENCY);
    FLASH->ACR |= (uint32_t)FLASH_ACR_LATENCY_2;    
 
    /* HCLK = SYSCLK */
    RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1; // this is AHB Prescaler
      
    /* PCLK2 = HCLK */
    RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1; // this is APB2 Prescaler
    
    /* PCLK1 = HCLK */
    RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV2; // this is APB1 Prescaler

    /* PLL configuration: HSE = 12MHz; PLLCLK = HSE * 6 = 72 MHz */
    /* Reset PLL entry clock source, HSE divider for PLL entry PLLMUL configuration and PLL multiplication factor */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLXTPRE |
                                        RCC_CFGR_PLLMULL));
    /* HSE clock selected as PLL entry clock source and PLL input clock*6 */
    RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_HSE | RCC_CFGR_PLLMULL6);

    /* Enable PLL */
    RCC->CR |= RCC_CR_PLLON;

    /* Wait till PLL is ready */
    while((RCC->CR & RCC_CR_PLLRDY) == 0)
    {
    }
    
    /* Select PLL as system clock source */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW)); // Reset System clock Switch
    RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;    // PLL selected as system clock SWS configuration 

    /* Wait till PLL is used as system clock source */
    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)0x08)
    {
    }
  }
  else
  { /* If HSE fails to start-up, the application will have wrong clock 
         configuration. User can add here some code to deal with this error */
  }
}
```
![Capture.JPG](https://bitbucket.org/repo/BAxXgB/images/3015037382-Capture.JPG)

d. Call **SystemInit()** at top main function. That's all :+1:
